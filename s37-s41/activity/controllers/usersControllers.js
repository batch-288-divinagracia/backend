const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js");

const bcrypt = require("bcrypt");

// require the auth.js
const auth = require("../auth.js")

// Controllers


// Create a controller for the signup
// registerUser
/* Business Logic/Flow */
	// 1. first, we have to validate whether the user is existing or not. We can do that by validating whether the email exist on our databases or not.
	// 2. if the user email is existing, we will prompt an error telling the user that the email is taken
	// 3. otherwise, we will sign up or add the user in our database.
module.exports.registerUser = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {
		// we need add if statement to verify whether the email exist already in our database.
		if(result){
			return response.send(false);
		} else {

			// Create a new object instantiated using the Users Model.
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				// hashSync method : it hash/encrypt our password
				// the second arguement is salt rounds
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo 
			})

			// Save the user
			// Error handling
			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(false));
		}
	}).catch(error => response.send(error));
}

// new controller for the authentication of the user
module.exports.loginUser = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false)
		} else { 
			// the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve form the find method. returns true or false depending on the results of the comparison.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)})
			} else{	
				return response.send(false)
			}
		}
	}).catch(error => response.send(false));
}

// activity s38 


module.exports.getProfile = (request, response ) => {

	const userData = auth.decode(request.headers.authorization);

	

	if(userData.isAdmin){

		return Users.findById(request.body.id)

		.then(result => {

					// Changes the value of the user's password to an empty string when returned to the frontend
					// Not doing so will expose the user's password which will also not be needed in other parts of our application
					// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
					result.password = "*******";


					// Returns the user information with the password as an empty string
					return response.send(result);

				});

	}else{

		return response.send(`You are not an admin, you dont't have access to this route.`);
		
	}

	
}


// Controller for the enroll course. 
module.exports.enrollCourse = async (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.body.id;

	if(userData.isAdmin){

		return response.send(false)
	} else {

		// push going to user document
		let isUserUpdated  = await Users.findOne({_id: userData.id})
		.then(result => {
			result.enrollments.push({courseId: courseId})

			return result.save()
			.then(saved => true)
			.catch(error => false)


		})
		.catch(error => false)


		// push going to course docuement

		let isCourseUpdated = await Courses.findOne({_id: courseId})
		.then(result => {

			result.enrollees.push({userId: userData.id})

			return result.save()
			.then(saved => true)
			.catch(error => false)

		})
		.catch(error => false)

		// If condition to check whether we updated the user documents and courses documents

		if( isUserUpdated && isCourseUpdated){
			return response.send(true);
		} else {

			return response.send(false)
		}

	}

}

module.exports.retrieveUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data));


}