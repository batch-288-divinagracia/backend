const Courses = require("../models/Courses.js")

const auth = require("../auth.js");

// this controller is for the Course Creation
module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){

		return response.send(false);
		
	} else{

	// create an object using the courses model
		let newCourse = new Courses({
			// supply all the required fields declared on the courses model.
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots

		})

		newCourse.save()
		.then(save => response.send(true))
		.catch(error => response.send(false))	
	}
}


// In this controller we are going to retrieve all ot the course in our database.

module.exports.getAllCourses = (request, response) => {

	const userData	= auth.decode(request.headers.authorization);

	if(userData.isAdmin	){

			Courses.find({})
			.then( result => response.send(result))
			.catch(error => response.send(false));

		} else {

			return response.send(false);
		}

}


// Route for retrieving all active courses

module.exports.getActiveCourses = (request, response) => {

	Courses.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// Controller will retrieve the information of a single documents using  the provided params.
module.exports.getCourse = (request, response) => {

	const courseId = request.params.courseId;

	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// this controller will update our document or course 
module.exports.updateCourse = (request, response) => {

	let userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	// updates that usually happen description, price, name
	
	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){

		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));

	} else {

		return	response.send(false);

	}

}


module.exports.archivingCourse = (request, response) => {

	let userData = auth.decode(request.headers.authorization)

	const courseId = request.params.courseId;

	let archiveCourse = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){

		Courses.findByIdAndUpdate(courseId, archiveCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));

	} else {

		return	response.send(false);

	}

}

// Controller for retrieving archived courses
module.exports.getInactiveCourses = (request, response) => {

	let userData = auth.decode(request.headers.authorization)

	if(!userData.isAdmin){

		 return response.send(false);

	} else {

		Courses.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(false));

	}
}