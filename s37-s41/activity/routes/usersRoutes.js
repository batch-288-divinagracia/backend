const express = require("express");
const usersControllers = require("../controllers/usersControllers.js")

const router = express.Router();

const auth = require("../auth.js")

// Routes

router.post("/register", usersControllers.registerUser);

router.post("/login", usersControllers.loginUser);

router.get("/details", auth.verify, usersControllers.getProfile);

router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

//Routes for course enrollment
router.post("/enroll", auth.verify, usersControllers.enrollCourse);


// tokenVerification
// router.get("/verify", auth.verify);






module.exports = router;

