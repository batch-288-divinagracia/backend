const express = require("express");

const coursesControllers = require('../controllers/coursesControllers');

const router = express.Router();

const auth = require("../auth.js")

// this route is responsible for adding course in our db.

router.post("/addCourse", auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses

router.get("/", auth.verify, coursesControllers.getAllCourses);

// route for retrieving all courses
router.get("/activeCourses", coursesControllers.getActiveCourses);

// Route for inactive courses
router.get("/inactiveCourses", auth.verify, coursesControllers.getInactiveCourses)

// route for getting the specific course information
router.get("/:courseId", coursesControllers.getCourse)

// route for updating course
router.patch("/:courseId", auth.verify, coursesControllers.updateCourse);

// route for archiving courses
router.patch("/:courseId/archive", auth.verify, coursesControllers.archivingCourse);



module.exports = router;