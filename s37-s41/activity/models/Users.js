const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "User fristName is required!"]
	},
	lastName: {
		type: String,
		required: [true, "User lastName is required!"]
	},
	email: {
		type: String,
		required: [true, "User email is required!"]
	},
	password: {
		type: String,
		required: [true, "User password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "User mobile number is required!"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "User course ID is required!"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	] 
})

const Users = mongoose.model("Users", userSchema);

module.exports = Users;