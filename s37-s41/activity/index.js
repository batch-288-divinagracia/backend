const express = require("express");
const mongoose = require("mongoose");
// it will allow our backend application to be available to our frontend application.
// it will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require("cors");

const usersRoutes = require("./routes/usersRoutes.js");

const coursesRoutes = require("./routes/coursesRoutes");

const port = 4001;

const app = express();

// mongoDB connection
// 

mongoose.connect("mongodb+srv://admin:admin@batch288divinagracia.5bsmxzx.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {useNewUrlParser: true,  useUnifiedTopology: true});

const db = mongoose.connection;

db.on("error", console.error.bind(console,"Error, can't connect to the database!"));

db.once("open",()=> console.log("We are now connected to the database!"))

// middlewares
app.use(express.json());

app.use(express.urlencoded({extended:true}));
// Reminder that we are going to use this for the sake of the bootcamp
app.use(cors());

// add the routing of the routes from the userRoutes
app.use("/users", usersRoutes);
// add the routing of the routes from the courseRoutes
app.use("/courses", coursesRoutes);


if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};