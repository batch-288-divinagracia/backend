// Single line Comment 

/*
	Multi Line Comment
	this is a comment
*/

// [section] syntax, statements and comments
// statements in programing, these are the instruction that we tell the computer to perform
// javescript Statement usually it end with semicolon(;)
// semicolon are not required in JS, but we will use it to help us prepare for the other stric language like java
// a Syntax in programing, it is the set of rules that describes how statements must be constructed
// all lines / block of code should be written in a specific or else the statement will not run

// [section] Variable
// it is used to contain/store data.
// any information that is used by an application is stored in what we call memory
// when we create a variables, certain portion of a device memory is given a "name" that we call "variables"

// declaring variable
// declaring variables - tells our device that a variable name is created and is ready to store data
	// syntax:
		// let/const variableName

let myVariable;

// by default if you declare a variable and did not initialize its value it will become "undefined"

// console.log() is useful for printing values of a variable or certain results of code into the google chromes browser's console
console.log(myVariable);

/*
	Guide in writting variable
		1. use the "let" keyword followed by the variable name of your choice and use the assginment operator (=) to assign a value
		2. variable names should start with a lowercase character, use camCase for multiple words.
		3. for constant variables, use 'const' keyword.
		4. variable names, it should be indicative (descriptive) of the value being stored to avoid confusion 

*/
 
 // declaring and initializing variables
// initializing variables - the instance when a variable is given its initial or starting value.
	// syntax
		// let/const variableName = value;

// example:
let productName = 'desktop computer';

console.log(productName);

let productPirce = 18999;
console.log(productPirce);

// in the context of certain applications, some variables/ information and should not change.
// in this example, the interest rate for a loan or saving account or mortgage must not change due to real world concerns.

const interest = 3.539;

// reassigning variable values
// reassigning a variable, it means changing it's initial or previous into another value
	// syntax
	// variableName = newValue

productName = 'laptop';
console.log(productName);

// the value of the variable declared using the const keyword can't be reassigned.

/*interest = 4.489
console.log(interest)
*/



// reassigning variables vs initializing variables
// declares a variable

let supplier
// initializing
supplier = "john smith tradings";
// reassigning
supplier = "uy's trading";

// declaring and initializing a variable
let consumer = "chris";

// reassigning
consumer = "topher";

// can u declare a const variable without initializing 
	// no. an error will occur

const driver= "warlon jay";


// var vs let/const keyword
	// var - is also used in declaring variables. but var is in an EcmaScript 1 version (1997)
	// let/const keyword was introduced as a new features in ES6(2015)

// what makes let/const different from var?
	// there are issues associated with variables declared/created using var, regarding hoisting
	// hoisting is JavaScript Default behavior of moving declaration to the top
	// in terms if variable and constants, keyword var is hoisted and let and const does not allow hoisting

	// example of hoisted:
	
	a = 5;

	console.log(a)

	var a;

	/*b = 6;
	console.log(b);
	let b;
*/

	// let/const local/global scope
	// scope essentially means where these variables are available or accessible for use 

	// let and const are block scoped
	// A block is a chunk of code bounded by {}. A block lives i an curly braces. anything within the braces are block

let outerVariable = "hello";

let globalVariable;

	{
		let innerVariable = "hello again";
		console.log(innerVariable);
		console.log(outerVariable);

		globalvariable = innerVariable;
		let globe = "hi!";
	}

	// console.log(innerVariable);

console.log(globalvariable);

// multiple variable declaration and initialization
// multiple variables  may be declared in one statement
let productCode = "DC017", productBrand = "Dell";

console.log(productCode);
console.log(productBrand);

// multiple variables to be consoled in one line.
console.log(productCode, productBrand)

//using a variable with a reserved keyword.
// reserved keyword cannot be used as a variable name as it has function in javaScript.

// console.log(let);

// [section] data types

// string
// string are a series of characters that create a word, a phrase, a sentence or anything related to creating text.
// string in JavaScript can be written using either a single('') or double("") quote

let country = 'philippines';
let province = "metro manila";

	// contenation of string in javaScript
	// Multiple string values can be combined to create a simple string using the "+" symbol  
	let	fullAddress = province +", " + country;
	console.log(fullAddress)

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	/*
		the escape characters (\) in string in combination with other characters can produce different effects/ results. - "\n" this creates a next line in between text.
	*/

	let mailAddress = 'Metro Manila\n\nPhilippines';
	console.log(mailAddress); 

	let message = "John's employees went home early."
	console.log(message);

	message = 'john\'s employees went home early.';
	console.log (message);

	// numbers
	// integers/whole numbers
	let headcount = 26;
	console.log(headcount);

	// decimal numbers/fractions
	let grade = 98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);

	// combining text and string
	console.log("john's grade last quarter is " + grade);

	// Boolean
	// Boolean values are normally used to create values relating to the state of certain things.
	let isMarried = false;
	let isGoodconduct = true;

	console.log("isMarried: " +isMarried);
	console.log("isGoodconduct: " + isGoodconduct);

	// Arrays
	// Arrays are special kind of data that's used to store multiple related values.

	// in JavaScript, Arrays can store different data types but is normally used to store similar data types.

	//similar data types
	//syntax
		//let/const arrayName = [elementA, elementB,elementC, ....];

	let grades = ["98.7, 92.1, 90.2, 94.6"];
	console.log(grades);


	// different data types
	let details = ["john", "smith", 32, true];
	// not recommended in using array
	console.log(details);

	// objects
	// objects are another special kind of data types that is use to mimic real world object/items


	/*

		syntax:
		let/const objectiveName = {
			propertyA: valueA
			propertyA: valueB
		}

	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}

	console.log(person);
	// type of operator, is used to determine the type of data or value of a variable. it outputs string
	console.log(typeof mailAddress);
	console.log(typeof headcount);
	console.log(typeof isMarried);

	console.log(typeof grades);

	// note: array is a special type of object with methods and function to manipulate. +

	// constant objective and arrays

	/*
	the keyword const is a little misleading

	it does not define a constant value. it defines a constant reference to a value
	because of this you can not:
	reassign a constant value.
	reassign a constant array.
	reassign a constant object.

	but you can:

	change the elements of a constant array.
	change the properties of constant object.
	*/

	const anime = ["one piece", "one punch man", "attack on titan"];

	/*anime = ["one piece", "one punch man", "kimetsu no yaiba"];
	console.log(anime);
*/

	anime[2] = "kimetsu no yaiba";
	console.log(anime);

	// null
	// it is used to intentionally express the absence of a value in a variable.

	let spouse = null;

	spouse = "Maria"; 


	// undefined
	// represent the state of a variable taht has been declared but witho ut an assigned value.

	let fullName;

	fullname = "Maria";












