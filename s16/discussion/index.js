console.log("Hello Batch 288!")
// to check whether the script file is properly linked to your html file.
// console.log(Hello Batch 288!);

// [Section] Arithmetic Operators
	let x = 1397;
	let y = 7831;

	// addition Operator (+)
	let sum = x + y;

	console.log("Result of addition operator: " + sum);

	// subtraction Operation(-)
	let difference = y - x;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication Operator(*)
	let product = x * y;
	console.log("Result of multiplicaiton operator: " + product);

	// division Operator
	let quotient = y / x;
	console.log("result of division operator: " + quotient.toFixed(2));

	// modulo Operator()
	let remainder = y % x;
	console.log('result of modulo operator: ' + remainder);

	// [section] assignment operator
	// assignment operator (=)
	// the assignment operator assigns/reassign the value to the variable

	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// the addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

	assignmentNumber += 2;

	console.log(assignmentNumber);

	assignmentNumber += 3;

	console.log("rusult of addition assignment operator: "+ assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator(-=, *=, /=)

	// subtraction assignment operator
	assignmentNumber -= 2;
	console.log("result of subtraction assignment operator: " + assignmentNumber);

	// multiplicaiton assignment operator
	assignmentNumber *= 3;
	console.log("Result of multiplicaiton assignment operator: " + assignmentNumber);

	// division assignment operator
	assignmentNumber /= 11;
	console.log("Result of multiplicaiton assignment operator: " + assignmentNumber);

// multiple Operators and parentheses 
	// MDAS - Multiplication or division first then Addition or Subtraction, from left to right.
	let mdas = 1 + 2 - 3 * 4 / 5;
	/*
		1.	3 * 4 = 12
		2.	12 / 5 = 2.4
		1 + 2 - 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	console.log(mdas.toFixed(1));

	let pemdas = 1 + (2-3) * (4/5);
	/*
		1. 4/5 = 0.8
		2. 2-3 = -1
		1 + (-1) * (0.8)
		3. -1 * 0.8 = -0.8
		1 - 0.8 = 0.2
	*/
	console.log(pemdas.toFixed(1));

// [section] Increment and Decrement 
	// Operators that add or subtract values by 1 and reassings the value of the variable where the increment and decrement applied to. 

	let z = 1;

	let increment = ++z;
	console.log("Result of pre-increment: " + increment);

	console.log("Result of pre-increment: " + z);

	 increment = z++;
	 console.log("the result of post-increment: " + increment);

	 console.log("the result of post-increment: " + z);

	 x = 1;

	 let decrement = --x;

	 console.log("Result of pre-decrement: " + decrement);;

	 console.log("Result of pre-decrement: " + x);

	 decrement = x--;
	 console.log("Result of post-decrement: " + decrement);
	 console.log("Result of post-decrement: " + x);


// [Section] Type Coercion
	 /*
		-type coercion is the automatic or implicit conversion of values from one data type to another.
		-this happens when operations are performed on different data types that would normally possible and yield irregular results.
	 */

	 let numA = '10';
	 let numB = 12;

	 let coercion = numA + numB;
	 console.log(coercion);
	 console.log(typeof coercion);

	 let numC = 16;
	 let numD = 14;

	 let nonCoercion = numC + numD;
	 console.log(nonCoercion);
	 console.log(typeof nonCoercion);

	 // the boolean "true" is also associated with the value of 1
	 let numE = true + 1;
	 console.log(numE);

	 // the boolean "false" is also associated with the value of 0
	 let numF = false + 1;
	 console.log(numF);

	 // [Selection] Comparison Operators

	 let juan = 'juan';

	 // Equality operator(==) 
	 /* 
		- check whether the operand are equal/have the same content/ value.
		- attempts to Convert and Compare operands of different data types.
		- returns a boolean value.

	 */

	 console.log(1 == 1); //true
	 console.log(1 == 2); //false
	 console.log(1 == '1'); //true
	 console.log(0 == false); //true
	 // compare two strings that are the same
	 console.log('JUAN' == 'juan'); //false (case-sensitive)
	 console.log(juan == 'juan'); //true

	 // Inequality Operator (!=)
	 /*
		- checks whether the operands are not equal/ have different content/values.
		- Attempts to Convert and Compare Operands of different data types.
		-returns Boolean Values
	 */

	 console.log(1 != 1); //false
	 console.log(1 != 2); //true
	 console.log(1 != '1'); //false
	 console.log(0 != false); //false
	 console.log('JUAN' != 'juan'); //true
	 console.log(juan != 'juan'); //false


	 // Strict Equality Operators(===)
	 /*
		-checks whether the operands are equal/ have the same content
		- Also Compare the data types of two values.
	 */

	 console.log(1 === 1); //true
	 console.log(1 === 2); //false
	 console.log(1 === '1'); //false
	 console.log(0 === false); //false
	 console.log('JUAN' === 'juan'); //false
	 console.log(juan === 'juan'); //true

	 // strictly Inequality operator (!==)
	 /*
		- checks whether the operands are not equal/have different content
		- Also Compares the data type of 2 values
	 */

	 console.log(1 !== 1); //false
	 console.log(1 !== 2); //true
	 console.log(1 !== "1"); //true
	 console.log(0 !== false); //true
	 console.log('JUAN' !== 'juan'); //true
	 console.log(juan !== 'juan') //false


// [Section] Relational Operators
	 //  checks whether one value is greater or less than to the other values.

	 let a = 50;
	 let b = 65;

	 // GT or Greater than Operator (>)
	 let isGreaterThan = a > b; //false
	 // LT or Less than Operator (<)
	 let isLessThan = a < b; //true
	 // GTE or Greater than or Equal Operator (>=)
	 a = 65
	 let isGTorEqual = a >= b; //false
	 // LTE or less than or Equal (<=)
	 let isLTorEqual = a <= b; //true

	 console.log(isGreaterThan);
	 console.log(isLessThan);
	 console.log(isGTorEqual);
	 console.log(isLTorEqual);


	 let numStr = "30";
	 console.log(a > numStr);
	 console.log(b <= numStr);

	 let strNum = "twenty";
	 console.log(b >= strNum);
	 // since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number)

	 // [section] logical Operators

	 let isLegalAge = true;
	 let isRegistered = false;

	 // logical AND operator (&& - double Ampersand)
	 //  Returns true if all operands are true 
	 // isRegistered = true
	 let allRequirementsMet = isLegalAge && isRegistered; //false
	 console.log("Result of logical AND operators: " +allRequirementsMet);


	 // logical OR operator (|| - double Pipe)
	 //  Returns "true" if one of the operands are true
	 // isLegalAge = false;
	 let someRequirementsMet = isLegalAge || isRegistered;
	 console.log("Results of logical OR operator: " + someRequirementsMet);

	 // Logical Not Operator (! - Exclamation Point)
	 // Returns the opposite value.

	 let someRequirementsNotMet = !isRegistered;
	 console.log("Result of Logical NOT operators: " + someRequirementsNotMet);









