//Add code here

let http = require('http');

let app = http.createServer(function (request, response) {

    if(request.url == "/"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Welcome to Booking System`)
    }

    else if(request.url == "/profile"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Welcome to your profile!`)
    }

    else if(request.url == "/courses"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Here's our courses available`)
    }

    else if(request.url == "/addCourse"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Add a course to our resources`)
    }

    else if(request.url == "/updateCourse"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Update a course to our resources`)
    }

    else if(request.url == "/archiveCourses"){
        response.writeHead(200, {"content-type": "text/plain"})
        response.end(`Archive courses to our resources`)
    } else {
        response.writeHead(404, {"content-type": "text/plain"})
        response.end(`Page not Found!`)
    }

})

app.listen(4000);


console.log("Server running at localHost: 4000");





//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
