/*console.log("Hello from your ex!");*/

// [Section] While Loop
	// A while loop takes an expression/condition. Expressions are any unit of code that can be evaluated as true or false. If the condition evaluates to be a true, the statements/code block will be executed.
	// A loop will iterate a certain number of times until an expression/condition is true.
	//Iteration is the term given to repitition of statements.
	/*
		Syntax: 
			while(expression/condtion){
				statement;
				increment/decrement;
			}
	*/

let count = 5;
	
	// While the value of count is not equal to zero the statement inside will run/iterate
	while( count !== 0 ){

		// The current value of count is printed out
		console.log( "While : " + count);

		// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
		//Forgetting to include this in our loops will make our applications run an infinite loop which will eventually crash our device.
		// After running the script, if a slow response from the browser is experienced or an infinite loop is seen int he console quickly close the application/browser/tab to avoid this.

		count--;
	}

// [Section] Do While Loop

	/*
		- A do while loop works a lot like the while loop. BUt unlike while loops, do-while loops guarantees that the code will be executed at least once.

		Syntax: 
			do {
				statement;
				increment/decrement;
			} while (condition/expression)
	*/
/*
let number = Number(prompt("Give me a number:"));
	// THe number function worls similarly to the "parseInt" function.
	
	do {
		console.log("Do While: " + number)

		// Increment
		number++;
	} while (number < 10)*/


//[Section] For Loop
	/*
		A for loop is more flexible than while and do-while loops. It consists of three parts:
			1. The "initialization" value that will track the progreession of the loop.
			2. expression/condtioon that will be evaluated which will determine whether the loop will run one more time.
			3. The "iteration" which indicates how to advance the loop.
			Syntax:
			for(initialization; expression/condition; iteration){
				statement;
			}
	*/

	/*
		Will create a loop that will start from 0 and will end at 20
		Every iteration of the loop, the value of count will be checked if it is equal or less than 20.
		If the value of count is less than or equal 20 the statement inside the loop will execute.
		The value of count will be incremented by one for each iteration.

	*/

	for(let count = 0; count <= 20; count++){
		console.log("The current value of count is " + count);
	}


let myString = "alexis";
// .length property
// Characters in strings may be counted using the .length property.


console.log(myString.length);

// Access elements of a string/ to the characters of the string.

console.log(myString[0]);
console.log(myString[3]);
// Since the last index in our string is 3, therefore the myString[5] will output an undefined.
console.log(myString[5]);

	//We will create a loop that will print out the individual letters of the myString variable;

	for(let index = 0; index < myString.length; index++){
		console.log(myString[index]);
	}


// Create a string named "myName" with value of Alex;
let myName = "Alex";
	/*
		Create a loop that will print out the letter of the name individually and printout the number 3 instead when the to be printed is a vowel.
	*/

for(let index = 0; index < myName.length; index++){

	/*console.log(myName[index]);*/
	if(myName[index].toLowerCase() === "a" ||
		myName[index].toLowerCase() === "e" ||
		myName[index].toLowerCase() === "i" ||
		myName[index].toLowerCase() === "o" ||
		myName[index].toLowerCase() === "u"){

		console.log(3);
	}else{
		console.log(myName[index]);
	}

}

let myNickName = "Chis Mortel";

let updatedNickName = myNickName.replace("Chis", "Chris");
console.log(myNickName);
console.log(updatedNickName);

// [Section] Continue and Break Statements
	// The "continue" statement allows the code to go to the next iteration of the loop wihtout finishing the execution of all statements in a code block.
	//The "break" statement is used to terminate the current loop once a match has been found.

// Create a loop that if the count value is divided by and the remain is 0, it will print the number and continue to the next iteration of the loop.
console.log("------------------------------------------")
	for(let count = 0; count <= 20; count++){

		if(count >=10){
			console.log("The number is equal to 10, stopping the loop");
			break;
		}

		if(count % 2 === 0){
			console.log('The number is divisible to two, skipping . . .')
			continue;
		}
		console.log(count);

	}

	// Create a loop that will iterate based on the length of the string name;

let name = "alexandro";
	// We are going to console the letters per line, and if the letter is equal to "a" we are going to console "Continue to the next iteration" then add continue statement.
	// If the current letter is equal to "d" we are going to stop the loop.

console.log("________________________________");
	for(let index = 0 ; index < name.length; index++){

		if(name[index] === "a"){
			console.log("Continue to the next iteration!")
			continue;
		}else if(name[index] === "d"){
			break;
		}else{
			console.log(name[index]);
		}

	}
