const http = require('http');

// creates a variable port to store the port number
const port = 8888;

const server= http.createServer((request, response) => {
	if(request.url == '/greetings'){
		response.writeHead(200, {'content-Type': "text/plain"})

		response.end('Hello Michael Jordan!')
	} else if( request.url == '/homepage'){
		response.writeHead(200 , {'content-Type': 'text/plain'})

		response.end('this is homepage')
	} else{
		response.writeHead(404, {"content-Type": "text/plain"})

		response.end('hey! page is not available')
	}
}) 

server.listen(port);

console.log(`server is now accessible at localhost:${port}.`);