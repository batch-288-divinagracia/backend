// use the "require" directive to load Node.js modules
// a module is a software component or part of a program that contains one or more routines
//  the "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
//  in that way, HTTP is a protocol that allows the fetching of resources such as HTML documents

// Clients (browser) and Server (Node JS/ Express JS Applications) communicate by exchanging individual messages.
// these message are sent by the clients, usually a web browser and called "request"

// the message sent by the server as an answer are called "response"
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client.
//  A port is a vertual point where network connections starts and end.
// the http module has a createServer method that accepts a function as an arguement and allows for create of a server 
// the arguements passed in the function are request and response object (data type) that contains methods that allows us to receive request from the client and send responses back to it. 

// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any request that are sent to it eventually communicating with our server
http.createServer(function (request, response) {

	// Use the writeHead() method to:
	// set a status code for the response - a 200 means OK
	// set the content-type of the response as a plain text message 
	response.writeHead(200, {'content-Type': 'text/plain'});

	// send the response with text content "Hello world?!"
	response.end("Hello World?!");

}).listen(8000);

// when the server is running, console will print the message:
console.log('Server running at localhost:8000');