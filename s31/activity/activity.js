// 1. What directive is used by Node.js in loading the modules it needs?

	// Answer:  the "require" directive is used to load Node.js modules

// 2. What Node.js module contains a method for server creation?

	// Answer: createServer()

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer: createServer method

// 4. What method of the response object allows us to set status codes and content types?
	
	// Answer:  writeHead() method

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer: In the gitBash console

// 6. What property of the request object contains the address's endpoint?

	// Answer: request.url