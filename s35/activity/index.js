const express = require('express');
// mongoose is a package which allows a creation of schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');

const port = 3001;

const app = express();

	// Section: MongoDB Connection
	// Connect to the database by passing your connection string
	// Due to update in MongoDB drivers that allows connection, the default connection string is being flagged as an error
	// by default a warning will be displayed in the terminal when the application is running.
	// {useNewUrlParser: true}

	// Syntax;
	// mongoose.connect("MongoDB string", {useNewUrlParser:true});

	mongoose.connect("mongodb+srv://admin:admin@batch288divinagracia.5bsmxzx.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser:true});

	// Notification whether we connected properly with the database.

	let db = mongoose.connection;

	// for catching the error just in case we had an error during the connection
	// console.error.bind allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

	// if the connection is successful:
	db.once("open", () => console.log("We're connected to the cloud Database!"));


	// [Section] Mongoose Schemas
	// Schemas determine the structure of the document to be written in the database
	// Schemas act as blueprint to our data
	// Use the Schema() constructor of the mongoose module to create a new Schema object

	// Middlewares
	// allow our app to read json data
	app.use(express.json())

	app.use(express.urlencoded({extended:true}))

	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// let us another field which is status
		status: {
			type: String,
			default: "pending"
		}
	})

	// [Section] Models
	// Uses Schema and are used to create/instanciate objects that correspons to the schema.
	// Models use Schema and they act as the middleman from the server (JS code) to our database.

	// To create a model we are going to use the model():

	const Task = mongoose.model('Task', taskSchema);

	// [Section] Routes

	// Create a POST route to create a new task
	// Create a new task
	// Business Logic:
		//  1. Add a functionality to check whether there are duplicate task
		//  if the task is existing in the database, we return an error
		//  if the task doest exist in the database, we add it in the database
		//  2. the task data will be coming from the request's body 

	app.post("/task", (request, response) => {
		//findOne method is a mongoose method that acts similar to "find" in mongoDB.
		// if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will return an empty object or null.
		console.log(request.body.name)

		Task.findOne({name: request.body.name})
		.then(result => {
			// We can use if statement to check or verify whethere we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!");
			} else {

				// Create a new task and save it to the data base.
				let newTask = new Task({
					name: request.body.name
				})

				// the save() method will store the information to the database
				// since the newTask was created from the Mongoose Schema and Task Model, it will be save in task collection
				newTask.save()

				return response.send('New task created!')
			}
		})
	})

	// Get all the task in our collection
	// 1. retrieve all the documetns
	// 2. if an error is encountered, print the error
	// 3. if no error/s is/are found, send a success status to the client and show the documents retrived 

	app.get("/task", (request, response) => {
		Task.find({}).then(result =>{
			return response.send(result);
		}).catch(error => response.send(error))
	})


	// activity S35

	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	})

	const User = mongoose.model('User', userSchema);

	app.post("/signup", (request, response) => {
		User.findOne({username: request.body.username})
		.then(result => {
			if(result !== null){
				return response.send("Duplicate username found");
			} else if (request.body.username && request.body.password ){ 
				let newUser = new User({
					username: request.body.username,
					password: request.body.password

				});
				newUser.save()
				.then(result => {
					console.log("success")
					return response.send("New user registered")
				}).catch(error => {
					console.log(error)
					return response.send(error)
				});

			} else{

				return response.send("BOTH username and password must be provided.");
			}
		})
	})





if(require.main === module){
	app.listen(port, () =>{
		console.log(`Server is running at port ${port}!`)
	})
}

module.exports = app;

