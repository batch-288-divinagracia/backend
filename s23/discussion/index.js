// console.log("HELLO WORLD")

// [Section] Objects
	//  - an object is a data type is used to represent real world objectives.
	//  - it is a collection of related data and / or functionalities
	//  structure of Object
		//  - key is what wer call the property of an object
		//  - value - is the value of the specific key or property
		// Syntax:
		/*
			let objectName = {
				keyA: valueA;
				keyB: valueB;
				...
			}
		*/


		let cellphone = {
			// key-value pairs
			name: 'Nokia 3210',
			manufactureDate: 1999
		}

		console.log('Result from creating objects using initializers/literal notation:');

		let exampleArray = [1, 2, 3, 4, 5,]

		console.log(exampleArray);
		console.log(typeof exampleArray);

		console.log(cellphone);
		console.log(typeof cellphone);

		// Creating objects using constructor function
			//  creates a reusable function to create several objects that have the same data structure.
		// this is useful for creating multiple instances/copies of an object.
		// - An instance is a concrete occurance which emphasizes on the distinct/ uniqe identity of it.
		/*
			Syntax:
			function ObjectName(valueA, valueB){
				this.keysA = valueA;
				this.keysB = valueb;
			}
		*/

		// the "this" keyword allows us to assign a new object's properties by associationg them with values recieved from a constructor's function parameters.
		function Laptop(name, manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;
		}


		// this is a unique instance of the laptop object
		/*
			- the "new" operator  creates an instance of an object
			- object and instances are often interchange because object literals (let object = {}) and instances (let object = new object) are distinc/unique objects
		*/

		let laptop = new Laptop('Lenovo', 2008);
		console.log('Result from creating object using constructors: ')
		console.log(laptop);

		// this is another unique instance of the laptop object
		let myLaptop = new Laptop('Macbook Air', 2022);
		console.log('Result from creating object using constructors: ')
		console.log(myLaptop);

		let oldLaptop = Laptop('Portal R2E CCMC', 1980);
		/*
			- the example above  invokes/calls the "Laptop" function instead of creating a new object instance
			- returns "undefined" without the "new" operator because "Laptop" function does not have a return statement.
		*/
		console.log('Result from creating object using constructors: ')
		console.log(oldLaptop);

		// Creating empty object
		let computer = {};
		let myComputer = new Object();

		// [Section] Accessing Object Properties

		// using the dot notation
		console.log('Result from dot notation: ' + myLaptop.name);

		// using square bracket notation
		console.log('Result from square bracket notation: ' + myLaptop['name']);

		// Accessing array objects
		/*
			- Accessing array elements can also be done using square brackets
			- Accessing object properties using the square bracket notation and array indexes can cause confusion
			- By using dot notation, this easily helps us differentiate accessing elements from arrays and properties from object
			- Object properties have names that make it easier to associate pieces of information
		*/

		let array = [laptop, myLaptop]

		//  May confused for accessing array indexes
		console.log(array[0]['name']);
		// Differentiate between accessing array and object properties
		// this tells us that us that array[0] is an object by using dot notation
		console.log(array[0].name);

		// [Section] initializing/Adding/Deleting/Reassigning Objects Properties

		/*
			- like any other variable in JavaScript, object may have their prioperties initialized/Added after the object was created/declared
			- this is useful for times when an object's properties are undertermined at the time of creating them
		*/

		let car = {};

		// initializing /adding object properties using dot notation
		car.name = 'Honda Civic';
		console.log('Result from adding properties using dot notation');
		console.log(car);

		// initializing /adding object properties using square bracket notation
		/*
			- while using the square bracket it will allow access to spaces when assigning property name to make it easier to read, this also make it so that object properties can only be accessed using the square bracket notation
			- this also makes name of object properties to not follow commonly used naming conventions for them
		*/

		car['manufacture date'] = 2019
		console.log(car['manufacture date'])
		console.log(car['manufacture Date'])
		console.log(car.manufactureDate);
		console.log('Result from adding properties using square bracket notation');
		console.log(car);

		// Deleting Object properties
		delete car['manufacture date']
		console.log('Result from deleting properties');
		console.log(car);

		// Reassigning object properties
		car.name = 'Dodge Charger R/T'
		console.log('Result from reasigning properties');
		console.log(car);

		// [Section] Object Methods
		/*
			- A method is a function which is a property of an object 
			- They are also functions and one of the key differences they have is that methods are function related to a specific functions which are used to perform task on them
			- Similar to functions/features of real world object, methods are defined based on what an object is capable of doing and how it should work
		*/

		let person = {
			name: 'John',
			talk: function(){
				console.log('Hello my name is' + this.name);
			}
		}

		console.log(person);
		console.log('Result from object methods: ');
		person.talk();

		// Adding methods to objects
		person.walk = function(){
			console.log(this.name + ' walked 25 steps forward')
		}
		person.walk();

		// methods are useful for creating reusable functions that perform task related to objects

		let friend = {
			firstName: 'Joe',
			lastName: 'Smith',
			address: {
				city: 'Austin',
				state: 'Texas'
			},
			emails: ['joe@mail.com', 'joesmith@mail.xyz'],
			introduce: function(){
				console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
			}
		}
		friend.introduce();
		console.log(friend);

		// [Section] Real world application of objects
		/*
			Scenario
				1. we would like to create a game that would have several pokemon interact with each other
				2. every pokemon would have the same set of stats, properties and functions
		*/

		let myPokemon = {
			name: 'Pikachu',
			level: 3,
			health: 100,
			attack: 50,
			tackle: function(){
				console.log("This pokemon tackled targetPokemon");
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			},
			faint : function(){
				console.log('Pokemon fainted');
			}
		}

		console.log(myPokemon);

		// Creating an object constructor will help in this process
		function pokemon(name, level){
			// properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level

			// methods
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name );
				console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
			}
			this.faint = function(){
				console.log(this.name + ' Fainted');
			}
		}

		//create new instances of "pokemon" object each with their unique properties
		let pikachu = new pokemon("Pikachu", 16);
		let rattata = new pokemon("Rattata", 8);


		// providing the "rattata" object as an arguement to the "pikachu" tackle method will create interaction between the two objects
		pikachu.tackle(rattata);

		



