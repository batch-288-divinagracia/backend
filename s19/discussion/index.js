console.log("Hello World")

// condition statements allows us to control the flow of our program. it allows us to run a statement/instruction if a 

let numA = -1;

// if statement
	// Executes a statement if the specified condition is met or true.

if(numA > 0){
	console.log('Hello');
}

/*
	syntax:
	if(condition){
		statement;
	}	
*/

// the result of the expression in the if's condition must result to true, else, the statement inside the {} will not run.

console.log(numA>0);

// let's update the variable and run an if statement with the same condition;
numA = 0;

if(numA < 0){
	console.log("hello again if numA is 0!");
}

// it will not run because the expression now results ti false:
console.log(numA < 0);

//  let's take a look at another example:

let city = "New York";

if( city === "New York"){
	console.log("Welcome to New York City!");
}

console.log(city === "New York");

// else if statement
	/*
		- executes a statement  if previous condition are false and if the specified condition is true
		-the "else if " statement is optional and can be added to capture additional conditions to change the flow of a program.
	*/

let numH = 1
if(numH > 2){
	console.log('Hello')
}
else if(numH < 2){
	console	.log("World");
}

// We were able to run  the else if() statement after we evaluated that the if condition false.

numH = 2
if(numH === 2){
	console.log("Hello");
}
else if( numA > 1){
	console.log("World");
}

//else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there.

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
}
else if(city === "Manila"){
	console.log("Welcome to Manila City, Ph!");
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// Since we failed the condition for the if() and the first else if(), we went to the second else if() and checked.

// else statement
	
/*	- executes a statement if all other condition are 		false. 
	- the "else" statement is optional and can be added to capture any other possible result to change the flow of a program.
*/

let numB = 0;
if(numB > 0){
	console.log("hello from numB!");
}
else if(numB < 0){
	console.log("World from numB!");
}
else{
	console.log("Again from numB");
}

	/*
		since the preceeding if and else if conditions failed, the else statement was run instead.
	*/
// It will cause an error
/*else{
	console.log("will not run without if!");
}*/

/*else if(numB ===2){
	console.log("this will not cause an error!")
}*/
// if, else if and else statement with function
	/*
		most  of the times we would like to use if, else  if and else statement with functions to control the flow of our program.
	*/

	// we  are going to create a function that will tell the typhoon intensity by providing the wind speed.


	function determineTyphoonIntensity(windSpeed) {
		if(windSpeed < 0){
			return "Invalid Wind Speed";
		}
		else if(windSpeed <= 38 && windSpeed >= 0){
			return "Tropical Depression Detected!";
		}
		else if(windSpeed >= 39 && windSpeed <= 73){
			return "Tropical Storm Detected"
		}
		else if(windSpeed >= 74 && windSpeed <= 95){
			return "Signal Number 1 Detected";
		}
		else if(windSpeed >= 96 && windSpeed <= 110){
			return "Signal Number 2 Detected";
		}
		else if(windSpeed >= 111 && windSpeed <= 129){
			return "Signal Number 3 Detected";
		}
		else if (windSpeed >= 130 && windSpeed <= 156) {
			return "Signal Number 4 Detected";
		}
		else{
			return "Signal Number 5 Detected";
		}
	}

		console.log(determineTyphoonIntensity(30))

		console.log(determineTyphoonIntensity(-1))
		console.log(determineTyphoonIntensity(157))

		// console.warn() is good way to print warning in our console that could tel us developers act on a certain output within our.
		console.warn(determineTyphoonIntensity(40))

// [section] truthy and falsy
	// In javaScript a truthy value is a value that is considered true when encounterd in a boolean context.
	// falsy values/ excemption for truthy:
		/*
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN

		*/

		// Truthy Examples

		if(true){
			console.log("Truthy");
		}

		if(78){
			console.log("Truthy");
		}

		// falsy Examples

		if(false){
			console.log("Falsy");
		}

		if(0){
			console.log("Falsy");
		}

// [Section] Contional (Ternary) Operator
		/*
			- The ternary operator takes in three operands:
				1. condition
				2.expression to execute if the condition is truthy
				3. expression to execute if the condition is falsy

		- it can be used to an if else statement
		- Ternary Operators have an implicit return statement meaning without "return" the resulting expression can be stored in a variable.

			- syntax:
				condition ? ifTrue : ifFalse

		*/

		// single statement execution
		let ternaryResult = (1 < 18) ? true : false;

		console.log("Result of Ternary Operator: " + ternaryResult);

		let exampleTernary = (0) ? "the number is not equal to zero" : "The number is equal to zero"
		console.log(exampleTernary);

		//Multiple statement Execution
		function isOfLegalAge(argument) {
			let name = "John"
			return ("You are in the legal age limit, " + name);
		}

		function isUnderAge() {
			let name = "John"
			return("You are under the legal age limit, " + name);
		}

		// let age = parseInt(prompt("What is your age?"));
		// console.log(age);
		// console.log(typeof age);
		// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();

		// console.log(legalAge);

		// the parseInt function converts the input received as a string data type into a number

		// [section] Switch Statement
		// switch statement evaluates an expression and matches the expression's value to a case clause. the switch will then execute the statement associated with the case, as well as statements in cases that follow the matching case.

		// the break statement is used to terminate the current loop once match has been found.

		/*
			syntax:
			switch (expression/variable){
				case value:
					statement;
					break;
				default:
					statement;
					break:
			}

		*/

		// the ".toLowerCase()" function will change the input received from the prompt into all lowercase letters 
		// let day = prompt("waht day of the week is today?").toLowerCase();

		// switch(day){
		// 		case 'monday':
		// 			console.log("the color of the day is red!");
		// 			break;
		// 		case 'tuesday':
		// 			console.log("the color of the day is orange!");
		// 			break;
		// 		case 'wednesday':
		// 			console.log("the color of the day is yellow!");
		// 			break;
		// 		case 'thursday':
		// 			console.log("the color of the day is green!");
		// 			break;
		// 		case 'friday':
		// 			console.log("the color of the day is blue!");
		// 			break;
		// 		case 'saturday':
		// 			console.log("the color of the day is indigo!");
		// 			break;
		// 		case 'sunday':
		// 			console.log("the color of the day is violet!");
		// 			break;

		// 		default:
		// 			console.log("Please input a valid day!");
		// 			break;
		// }

		// [Section] try - Catch - Finaly Statement
			// try catch statement are commonly used for error handling
			// there are instances when the application returns an error/warning that is not necessarily an error in context of our code.

		function showIntensityAlert(windSpeed){
			try{
				alert(determineTyphoonIntensity(windSpeed))
			}
			// the catch will only run if and only if there was an error in the statement inside our try
			catch(error){
				console.log(typeof error);

				console.warn(error.message);
			}
			finally{
				// continue execution of code regardless of success and failure of code execution in the try block.
				alert("Intensity updates will show new alert!");
			}
		}

		showIntensityAlert(56);

		console.log("Hi Im after the show Intensity alert");

