 // console.log("TGIF!")

// [Section] Exponent Operator
	// Before the ES6 update
	// Math.pow(base, exponents);

	// 8 raise to the power of 2
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	const secondNum = Math.pow(5,5);
	console.log(secondNum);

	// after the ES6

	const thirdNum = 8**2;
	console.log(thirdNum);

	const fourthNum = 5**5;
	console.log(fourthNum);

// [Section] Template Literals
	// it will allow us to write strings without using the concatenation operator
	// greatly helps with the code readability.

	// before ES6
	let name = "John";
	let message = 'Hello ' + name + '! Welcome to programming!';
	console.log(message);

	// after the ES6 updage
	// Uses backticks (``)

	message = `Hello ${name}! Welcome to programming!`;
	console.log(message)

	console.log(typeof message);

	// multi-line using Template literals
	const anotherMessage = `${name} attended a math competition.
	He won it by solving the problem 8**2 with the solution of ${firstNum}`;
	console.log(anotherMessage);

	const interestRate = .1;
	const principal = 1000;

	// template literals allows us to write string with embedded JavaScript expression
	// expressions are any valid unit of code that resolves to a value
	// "${}" are used to include JavaScript expression in string using the template literals
	console.log(`the interest on your savings account is : ${interestRate * principal}`)

// [Section] Array Destructuring
	/*
		- allows us to unpack elements in array into distinct variables
		Syntax:
			let/const [VariableNameA, variableNameB, variableNameC, . . .] = arrayName
	*/

	const fullName = ["Juan", "Dela", "Cruz"];

	// before the ES6 update
	let firstName = fullName[0]
	let middleName = fullName[1]
	let lastName = fullName[2]

	console.log(`Hello ${firstName} ${middleName} ${lastName}! it's nice to meet you`);

	// after the ES6 update:
	// const [nickName, secondName, familyName] = fullName;
	// console.log(nickName);
	// console.log(secondName);
	// console.log(familyName);

	/*nickName = "pedro";
	console.log(nickName);*/

	// Another Example of array Destructuring;
	let gradesPerQuarter = [98, 97, 95, 94];

	let [firstGrading, secondGrading, thirdGrading, fourthGrading] = gradesPerQuarter;
	// unlike the const keyword, this won't cause any error.
	firstGrading = 100;
	console.log(firstGrading);

	console.log(firstGrading);
	console.log(secondGrading);
	console.log(thirdGrading);
	console.log(fourthGrading);

// [Section] Object Destructuring
	// allow us to unpack properties of object into distinct variables;
	// shortens the syntax for accessing properties from objects
	/*
		Syntax:
			let/const {propertyNameA propertyNameB} = objectName;
	*/

	// before the ES6
	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	console.log(person);

	/*const givenName = person.givenName;
	const maidenName = person.maidenName;
	const familyName = person.familyName;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);*/

	//After the ES6 update


	// the order of the property/variable does not affect its value
	const {maidenName, familyName, givenName} = person;

	 console.log(`this is the givenName ${givenName}`);
	 console.log(`this is the maidenName ${maidenName}`);
	 console.log(`this is the familyName ${familyName}`);

// [Section] Arrow Function
		// Compact alternative syntax to a traditional functions

		/*
			Syntax:
				const/let variableName = () => {
					statement/codeblock;
				}

		*/
	 // arrow function without parameter
	 const hello = () => {
	 	console.log("hello world from the arrow function")
	 }

	 hello();

	 // arrow function with parameter
	 /*
			Syntax:
				const/let variableName = (parameter) => {
					statement/Codeblock;
				}

	 */

	 const printFullName = (firstName, middleInitial, lastName) => {
	 	console.log(`${firstName} ${middleInitial} ${lastName}`);

	 }

	 printFullName("John", "D", "Smith");

	 // Arrow function can also be used with loops
	 // Examples

	 const students = ["john", "jane", "judy"];

	 students.forEach((student) => {
	 	console.log(`${student} is a student.`)
	 });

// [Section] Implicit returns in Arrow function
	 // Example:
	 //  if the function will run one line or one statement, the arrow function will implicitly return the value
	 	let add = (x,y) => x+y;
	 	let total = add(10, 12);

	 	console.log(total);

	 	function trialFunction(x, y) { 
	 		return x+y
	 	};

	 	console.log(trialFunction(1,2))

// [Section] Default Function Arguement value
	 	// provides a default function arguement value if none is provided when the function is invoked.

	 	const greet = (name = "User", age = 0) => {
	 				return `Good morning, ${name}! I am ${age} years old!`;
	 			}

	 			console.log(greet( "russell", 78));

	 			function addNumber(x = 0, y = 0){
	 				console.log(x);
	 				console.log(y);
	 				return x + y;
	 			}

	 			let sum = addNumber(y=1);
	 			console.log(sum);




// [Section] Class-Based Object Blueprints
		//allow us to create /instantiate of object using a class as blueprint

		// Syntax:
		/*
		class className{
				constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectValueA;
				this.objectPropertyB = objectValueB;
				}
			}
		*/

		class Car {
			constructor(brand, name, year){
				this.brand = brand;
				this.name = name;
				this.year = year;

				this.drive = () => {
					console.log(`the car is moving 150km per our`)
				}
			}
		}

		// instantiate an object

		const myCar = new Car('Toyota', 'Civic', 2019);

		console.log(myCar);

		const myNewCar = new Car();

		console.log(myNewCar);

		myCar.drive();

