// [Section] mongoDB aggregation

/*
	-used to generate manipulated data and perform operations to create filtered results that helps analyzing data.
	-compare to doing the CRUD operations on our data from previous sessions, aggregation gives us to access to manipulative, filter and compute for results providing us with information to make necessary development decissions without having to create a frontend application.
*/
	// Using aggregate method

	db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id : "$supplier_id", total: {$sum: "$stock"}}}
	])

	db.fruits.aggregate([
		{ $match : { onSale: true}}
	])	

// the $group groups the documents in terms of the property declared in the _id property.
	db.fruits.aggregate([
	{ $group: {_id : "$supplier_id", sum: { $sum: "$stock"}}}
	])

	db.fruits.aggregate([
	{ $match: {onSale : true}},
	{ $group: {_id: "$supplier_id", max : {$max : "$stock"}, sum : {$sum: "$stock"}}}
	])

	db.fruits.aggregate([
	{ $match: {onSale: true}},
	{ $group: {
		_id: "$color", 
		max : {$max: "$stock"}, 
		sum : {$sum: "$stock"}}}
	])

//  Field Projection with aggregation
	/*
		- the $projection operation can be used when aggregating data to exclude the returned result
	*/

	db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {
			_id: "$color", 
			max : {$max: "$stock"}, 
			sum : {$sum: "$stock"}
		}},
		{ $project: {_id: 0}}
	])

// Sorting aggregated results
	/*
		the sort operator can be used to change the order of aggregated results
		providing value of -1 will sort the aggregated results in reverse order

		1 descending
		-1 ascending

		Syntax: 
			{sort : {field: 1 / -1}}
	*/

	db.fruits.aggregate([
	{ $match : {onSale: true}},
	{ $group : {
		_id: "$supplier_id",
		total : { $sum: "$stock"}
	}},
	{$sort: {total: 1}}
	])

	db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {
		_id: "$name",
		stock: {$sum: "$stock"}
	}},
	{ $sort: {_id : -1}}
	])

	db.fruits.aggregate([
	{$group: {
		_id: "$onSale",
		stock: {$sum: "$stock"}
	}},
	{ $sort: {_id : -1}}
	])


	db.fruits.aggregate([
	{ $group :{
		_id: "$color",
		stocks: {$sum : "$stock"}
	}},
	{ $project: {_id: 0}},
	{ $sort : {_id: 1}}
	])

	// Aggregating results based on an array field 
		// unwind operator

	/*
		- the $unwind operator deconstructs an array field from a collection/field with an array value to output a result  for each element 
		Syntax:
		{$unwind : field}
	*/

	db.fruits.aggregate([
		{ $unwind: "$origin"}
	])

// Display fruit documents by their origin and the kinds of fruit that are supplied

	db.fruits.aggregate([
		{$unwind: "$origin"},
		{$group : {
			_id: "$origin",
			kinds : {$sum: 1}
		}},
		{$sort: {kinds: 1, _id: 1}}
	])