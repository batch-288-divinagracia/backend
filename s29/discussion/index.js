// Advanced Queries

// Query an embedded document / object
db.users.find({
	contact : {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
})

db.users.find({
	contact : {
		email: "stephenhawking@gmail.com"
	}
});

// Dot notation
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
});

//querying an array with exact element
db.users.find( {
	courses: ["CSS", "Javascript", "Python"]
});

db.users.find({courses: {
	$all: ["React", "Laravel"]
}});

db.users.find({courses: {
	$all: ["React", "Sass"]
}});

//Query Operators

// [Section] Comparison Query Operators
	//$gt/$gte operator
		/*
			-allows us to find documents that have field number values greater or equal to a specified value
			-Note that this operator will only work if the data type of the field is number or integer.
			Syntax:
			db.collectionName.find({ field: {
				$gt: value
			}})
			db.collectionName.find({ field: {
				$gte: value
			}})
		*/

db.users.find({age : { $gt: 76}});

db.users.find({age : { $gte: 76}});
	//$lt/$lte operator
		/*
			- allows us to find documents that have field number values less than or equal to a specified value
			-Note: same with $gt/gte operator, this will only work if the data type of the field being queried is number or Int.
			Syntax:
			db.collectionName.find({field: {$lt : value}});
			db.collectionName.find({field: {$lte : value}});
		*/

db.users.find( { age : { $lt : 76}});
db.users.find( { age : { $lte : 76}});

		// $ne Operator
		/*
			- allows us to find documents that have field number values not equal to specified value
			Syntax:
			db.collectionName.find({field: {$ne : value}});
		*/

db.users.find({age : {$ne : 76}});

	// $in operator
/*
	- allows us to find documents with specific match cirteria one field using different values.
	Syntax:
		db.collectionName.find({
			field:
			{
				$in : value
			}
		})
*/

db.users.find({lastName : { $in : ["Hawking", "Doe", "Armstrong"]}});
db.users.find({lastName : "Hawking"});
db.users.find({lastName : "Doe"})


db.users.find({"contact.phone" : { $in : ["87654321"]}});

//  Using in operator in an array
db.users.find({ courses : { $in : ["React"]}});

db.users.find({ courses : { $in : ["React", "Laravel"]}});

// [Section] Logical Query Operators
	// $or Operator
	/*
		- allows us to find documents that match a single criteria from multiple provided search criteria
		Syntax:
			db.collectionName.find({
				$or : [{fieldA: valueA}, {fieldB: valueB}]
			})
	*/

db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

// add multiple operators
db.users.find({
	$or: 
	[
		{firstName: "Neil"}, 
		{age: { $gt : 25}}, 
		{courses: {$in: ["CSS"]}}
		]
});

// $and operator
/*
	-allows us to find documents matching all the multiple criteria in a single field.
	Syntax:
		db.collectionName.find({
		$and:
		[
			{fieldA: valueA},
			{fieldB: valueB},
			. . . .
		]
	})
*/
db.users.find({
	$and:
	[
		{ age: { $ne: 82}},
		{ age: { $ne: 76}}
		]
});

//  mini-activity
	//  you are going to query all the documents from the users collection, that follow these criteria:
		// information of the documents older than 30 that is enrolled in CSS or HtML. 

db.users.find({
	$and : 
	[
		{age: {$gt: 30}},
		{courses: {$in: ["CSS", "HTML"]}}
		]
});

// [Section] Field Projection
	/*
		- retriving documents are common operations that we do and by default mongoDB
	*/

// Inclusion
	/*
		-allows us to include or add specific fields only when retrieving documents:

		Syntax:
		db.collectionName.find({criteria},{field: 1});
	*/

db.users.find(
	{ firstName : "Jane"},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1,
		_id:0
	}
);

// Exclusion
/*
	allows us to exclude/ remove specific fields only when retrieving documents
	Syntax: 
		db.users.find({criteria}, {field: 0})
*/

db.users.find(
	{ firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
)

db.users.find(
	{firstName: "Jane"},
	{
		"contact.email": 0,
		department: 0
	}
	);

db.users.find(
{ $or:
	[{firstName: "Jane"},
		{age: {$gt: 30}}
		]
	},
	{
		"contact.email": 0,
		department: 0,
		courses: { $slice : 2}
	}
);

// Evaluattion Query Operator
	// $regex operator
		/*
			- allow us to find documents that match a specific string pattern using regular expressions.
			Syntax:
			db.collectionName.find({
					field: $regex: 'pattern', option : 'optionValue'
			})
		*/
// Case Sensitive
db.users.find({ firstName: {$regex: 'n'}});

// Case insensitive
db.users.find({ firstName: {$regex: 'n', $options: 'i'}});

