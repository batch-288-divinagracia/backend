
async function findName(db) {
	return await(

			
		db.users.find({
			$or : 
			[
				{firstName: {$regex: 's'}},
				{lastName: {$regex: 'd'}}
				]
			},
			{
				firstName:1,
				lastName:1,
				_id:0
			}
		)

		);

};

async function findDeptAge(db) {
	return await (

			
		db.users.find({
			$and:
			[
				{department: "HR"},
				{age: {$gte: 70}}
			]
		})

		);

};

async function findNameAge(db) {
	return await (

			
		db.users.find({
			$and: 
			[
				{lastName: {$regex: 'e'}},
				{age : {$lte: 30}}
			]
		})

		);
};


try{
    module.exports = {
        findName,
        findDeptAge,
        findNameAge
    };
} catch(err){

};

