const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routers/taskRoutes.js")

const port = 4000;

const app = express();

// Set up MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin@batch288divinagracia.5bsmxzx.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser:true});

// Check whether we are connected with our DB
const db = mongoose.connection;

db.on("error", console.error.bind(console, "Error, can't connect to the database!"));

db.once("open", ()=> console.log("We are now connected to the database!"));

// Middlewares

app.use(express.json());

app.use(express.urlencoded({extended:true}));


app.use("/task", taskRoutes);

if(require.main===module){
	app.listen(port, () => console.log(`The Server is running at port${port}!`));
}

module.exports = app;



// model > controller > router >server
