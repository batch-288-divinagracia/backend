const express = require("express");
const taskControllers = require("../controllers/taskControllers.js");

// Contains all the endpoints of our application
const router = express.Router()

router.get("/", taskControllers.getAllTasks);

router.post("/addTask", taskControllers.addTasks)

// parameterizer
// we are creating a route using a Delete method at the URL "tasks/:id"
// the colon is an identifier that helps to create a dynamic route which allows us to supply information
router.delete("/:id", taskControllers.deleteTask)

// Activity s36

router.get("/:id", taskControllers.getSpecificTask)

router.put("/:id/complete", taskControllers.changingTheStatusOfATaskToComplete)

module.exports = router;