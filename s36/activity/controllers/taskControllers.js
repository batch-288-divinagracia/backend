const Task = require("../models/task.js");

/*Controllers*/

// this controller will get/retrieve all the documetns from the task collection

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error)
	})
}

// Create a controller that will find a data in our database.
module.exports.addTasks =(request, response) => {
	Task.findOne({name: request.body.name})
	.then(result => {
		if(result !== null){
			return response.send("Duplicate Task");
		} else{

			let newTask = new Task({
				"name": request.body.name
			})

			newTask.save();
			return response.send("New task created!");
		}
	}).catch(error => response.send(error))

}

module.exports.deleteTask = (request, response) => {
	console.log(request.params.id);

	let taskToBeDeleted = request.params.id

	// In mongoose, we have findByIdAndRemove method that will look for a document with the same id provided from the URL and remove/ delete the document from MongoDB
	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted!`)
	}).catch(error => response.send(error))
}

// activity s36

module.exports.getSpecificTask = (request, response) => {

	let taskToFind = request.params.id

	Task.find({_id: taskToFind})
	.then(result => {
		return response.send(result)
	}).catch(error => response.send(error))
}

module.exports.changingTheStatusOfATaskToComplete = (request, response) => {

	let taskToChange = request.params.id
	console.log(taskToChange)

	Task.updateOne({_id: taskToChange}, {$set: {status: "complete"}})
	.then(update => {
		Task.findOne({_id: taskToChange})
		.then(result => {	
			return response.send(result)	
		})
	}).catch(error => response.send(error))
}