console.log("Good game Lakers!")

// [SECTION] JSON Objects
	//JSON stands for JavaScript Object Notation
	//JSON is also used in other programming languages hence the name JavaScript Object Notation.
	//Core JavaScript has a built in JSON object that containes methods for parsing objects and converting strings into JavaScript objects.
	//.json file, JSON file
	//JavaScript objects are not to be confused with JSON
	//JSON is used for serializing different data types into bytes
	//Serialization is the process of converting data into series of bytes for easier transmission/ tranfer of information/data.
	//A byte is a ubit of data that is eigth binary digits( 1 , 0) that is used to represent a character (letter, numbers, typographic symbols.)
	/*
		Syntax: {
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
	*/

// [Section]  JSON Arrays
/*
	"cities" : [
		{"city": "Quezon City", "province":"Metro Manila", "country" : "Philippines"}
	]

*/

// [Section] JSON Methods

// the JSON object contains methods for passing and converting data into stringified JSON.

// Converting data into stringified JSON
	// stringified JSON is a javaScript object converted into a string to be used in other functions of JavaScript application
	// theare Commonly used in HTTP request where information is requuired to be sent and receive in a stringified version
	// Requests are an important part of programming where an application communicaties task such as getting/creating data in a database.

	let batchesArr = [ {batchName: 'Batch X'}, {batchName: 'Batch Y'} ]

	console.log(batchesArr);
	console.log(typeof batchesArr);

	// stringify method is used to convert JavaScript Object into a string.
	console.log('Results from stringify method');
	// if you are going to convert objects to string, you'll used JSON.stringify(variableNAme);
	console.log(JSON.stringify(batchesArr));
	console.log(typeof JSON.stringify(batchesArr));

	// Using stringify method with variables
		/*
			JSON.stringify({
				propertyA: variableA
				probertyB: variableB
			})

		*/

	// User details
	// let firstName = prompt('What is your first Name?');
	// let lastName = prompt('What is your last name?');
	// let age = prompt('What is your age?');
	// let address = {
	// 	city: prompt('which city do you live in?'),
	// 	country: prompt('which country does your city address belong to?')
	// };

	// let otherData = JSON.stringify({
	// 	firstName: firstName,
	// 	lastName: lastName,
	// 	age: age,
	// 	address: address
	// });

	// console.log(otherData);

	// Converting stringified into JavaScaript objects
	// objects are common data types used in application because of the complex data structures that can be created out of them

	let batchesJSON = '[{"BatchNumber": 199}, {"batchNumber": 200}]';

	console.log(batchesJSON);

	console.log('Results from parse Method:');
	console.log(JSON.parse(batchesJSON));

	let exampleObject = '{"name" : "chris", "age": 16, "isTeenager": false }';


	console.log(JSON.parse(exampleObject));

	let exampleParse = JSON.parse(exampleObject);

	console.log(typeof exampleParse.name);
	console.log(typeof exampleParse.age);
	console.log(typeof exampleParse.isTeenager);
