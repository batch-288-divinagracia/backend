// console.log("Hello world")

// Array Methods
	// javaScript has built-in functions and methods for arrays. this allows us to manipulate and access array items.
	
	// Mutator Methods
	// Mutator methods are function that "mutate" or change an array after they're created
	//  These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];

	// push()

	/*
		- add an element  in the end of an array and returns the updated array's length
		syntax:
			arrayName.push();

	*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');


	console.log(fruitsLength)
	console.log("Mutated array from push method: ");
	console.log(fruits);

	// pushing mutiple elements to an array

	fruits.push('Avocado', "Guava");
	console.log('Mutated array after pushing multiple elements:')
	console.log(fruits)


	// pop()
	/*
		-removes the last element and return the remove element
		Syntax:
			arrayName.pop();

	*/

	console.log("current array: ");
	console.log(fruits);

	let removeFruit = fruits.pop();

	console.log(removeFruit);
	console.log('Mutated Array from pop method:');
	console.log(fruits);

	// unshift()\

	/*
		- it adds one or more elements at the beginning of an array 
		-syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', "elementB" . . . );

	*/

	console.log('current array');
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');

	console.log(fruitsLength);
	console.log('Mutated array from unshift method: ');
	console.log(fruits);

	// shift()
	/*
		- removes an element at the beginning of an array
		AND returns the removed element
		-Syntax:
			arrayName.Shift();

	*/

	console.log('current array');
	console.log(fruits);

	removeFruit = fruits.shift();

	console.log(removeFruit);
	console.log('Mutated array from shift method: ');
	console.log(fruits);

	// splice()
	/*
		-simultaneously removes an element from a specified index number and adds element
		-syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

	*/

	console.log("current array: ");
	console.log(fruits);

	fruits.splice(fruits.length, 0, "Cherry");
	console.log('Mutated array after the splice method: ');
	console.log(fruits);

	// Sort();

	/*
		Rearranges the array elements in alphanumeric order
			-Syntax:
				arrayName.sort();

	*/

	console.log('Current Array');
	console.log(fruits);

	fruits.sort();

	console.log("Mutated Array after the sort method: ");
	console.log(fruits);

	// reverse()
	/*
		-reverse the order of array elements
		-syntax:
			arrayName.reverse();
	*/

	console.log('Current Array');
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated Array after the reverse method: ");
	console.log(fruits);


	// [Section] Non-mutator Methods

	/*
		- non-mutator methods are functions that do not modify or change an array after the're created
		- these methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output.

	*/


	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE',];

	// indexOf()
	// returns the index number of the first matching element found in an array.
	//  if no match was found, the result will be -1.
	// the search process will be done from the first element proceeding to the last element
	/*
		syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, startingIndex);

	*/

	let firstIndex = countries.indexOf('PH');
	console.log(firstIndex);


	let invalidCountry = countries.indexOf('BR');
	console.log(invalidCountry);

	firstIndex = countries.indexOf('PH', 2);
	console.log(firstIndex);

	console.log(countries);

	// lastIndexOf
	/*
		- returns the index number of the last matching element found in an array.
		- the search process will be done from last element proceeding to the first element
			syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, startingIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf('BR');
	console.log(invalidCountry);

	lastIndexOf = countries.lastIndexOf('PH', 6);
	console.log(lastIndexOf);

	// indexOf, starting from the starting index going to the last element(from left to right);
	// lastIndexOf, starting from the starting index going to the first element(from right to left);

	console.log(countries);

	// slice()
	/*
	- portions/slices elements from an array AND return a new array
	- Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
	*/

	// slicing off elements from a specified index to the last element

	let slicedArrayA = countries.slice(2);
	console.log("result from slice method: ");
	console.log(slicedArrayA);

	// slicing off elements from a specified index to another index:
	// the elements that will be slice are elements from the starting index until the elements before the ending index.
	let splicedArrayB = countries.slice(2,4);
	console.log("result from slice method: ");
	console.log(splicedArrayB);

	// Slicing off elements starting from the last element of an array:
	let slicedArrayC = countries.slice(-5);
	console.log("result from slice method: ");
	console.log(slicedArrayC);

	// toString()
	/*
		returns an array as string separated by commas
			Syntax:
				arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("result from toString method: ");
	console.log(stringArray);
	console.log(typeof stringArray)

	// concat()
	// combines arrays to an array or elements and returns the combined results.

	// Syntax:
		// arrayA.concat(arrayB);
		// arrayA.concat(elementA);

	let taskArrayA = ['drink HTML', 'eat javascript'];
	let taskArrayB = ['inhale CSS', 'breathe sass'];
	let taskArrayC = ['get git', 'be node'];

	let task = taskArrayA.concat(taskArrayB);
	console.log(task);

	let combinedTask = taskArrayA.concat('smell express', 'throw react');
	console.log(combinedTask);

	// concat multiple array into an array

	let allTask = taskArrayA.concat(taskArrayB,taskArrayC);
	console.log(allTask)

	// concat arrat to an array and element
	let exampleTask = taskArrayA.concat(taskArrayB, "smell express")

	console.log(exampleTask);

	// join()
	// return an array as string separated by specified separator string.
	// Syntax:
		// arrayName.join('separatorsString')

	let users = ['john', 'jane', 'joe', 'Robert'];

	console.log(users.join());
	console.log(users.join('  '));
	console.log(users.join(" - "));

	// [Section] Iteration Methods
	// Iteration methods are loops designed to perform repititive task
	// Iteration methods loops over all items in an array

	// forEach()
	// similar to a for loop that iterates on each of array element.
	// Syntax:
		// arrayname.forEach(function(indivElement){statement};)

	console.log(allTask);
	// ['drink HTML', 'eat javascript', 'inhale CSS', 'breathe sass','get git', 'be node']



	allTask.forEach(function(task){
		console.log(task);
	});


	// filteredTask variables will hold all the elements from the allTask array that  has more than 10 characters.
	let filteredTask = [];

	allTask.forEach(function(task){
		if(task.length > 10){
			filteredTask.push(task);
		}
	})
	console.log(filteredTask);

	// map()
	// Iterates on each element and returns new array with different values depending on the result of the function's operation.

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		return number * 3;
	})
	console.log(numbers);
	console.log(numberMap);

	// every()
	/*
		it will check if all elements in an array meet the given condition
		- return true value if all elements meet the condition and false otherwise.

		Syntax:
			let/Const resultArray =arrayName.every(fucntion(indivElement){ return expression/condition;

			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function(number){
		return (number<6);
	})

	console.log(allValid);

	// some()
	// checks if at least one element in the array meets the given condition.
	/*
		Syntax:
			let/const resultArray = arrayName.some(function(indivElement){ return expression/condition;

			})
	*/

	let someValid = numbers.some(function(number){
		return	(number < 2);
	})

	console.log(someValid);

	// filter()
	// returns new array that contains elements which meets the given
	//  return an empty array if no elements were found
	/*
		Syntax:
			let/const resultArray = arrayname.filter(function(indivElement){ return expression/condition;

			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let filterValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})
	console.log(filterValid);

	// includes()
		// check if the arguement passed can be found in the array

	let products = ['mouse', 'keyboard', 'laptop', 'monitor'];

	let productFound1 = products.includes('mouser');
	console.log(productFound1);

	// reduce()
		// it evaluates elements from left to right and returns/ reduces the array into a single value

	numbers = [1, 2, 3, 4, 5];


	// the first parameter in the function will be the accumulator

	// the second parameter in the function will be the currentValue
	let reduceArray = numbers.reduce(function(x, y){
		// console.log('accumulator: ' + x);
		// console.log('currentValue: ' + y);
		return x + y;

	})
	console.log(reduceArray);

	products = ['mouse', 'keyboard', 'laptop', 'monitor'];

	let reducedArrayString = products.reduce(function(x, y){
		return (x + y);
	})
	console.log(reducedArrayString);


