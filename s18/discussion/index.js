// console.log("Hello World");

	function printInput() {
		let nickname = "chris";

		console.log("Hi, " + nickname);
	}

	printInput();
	printInput();

	//  for other cases, function can also process data directly passed into it insted of relying on global variable

	//  Consider this function
		function printName(name) {
			console.log("My name is " + name);
		}

		printName("Juana");

		printName("Chris");

		// variables can also be passed as an argument

			let sampleVariable = "Curry";

			let sampleArray = ["Davis", "Green", "Jokic", "Tatum"];

			printName(sampleVariable);
			printName(sampleArray);

		// one example of using the parameter and argument.
			// functionName(number);

			function checkDivisibilityBy8(num) {
				let remainder = num % 8;

				console.log("the remainder of " + num + " divided by 8 is: " + remainder);

				let isDivisibleBy8 = remainder === 0 ;

				console.log("is " + num + "divisible by 8?");
				console.log(isDivisibleBy8);

			}

			checkDivisibilityBy8(64);

			// functions as argument
				// function parameters can also accept other functions as arguments.

			function argumentFunction() {
				console.log("this function was passed as an argument before the message was printed");
			}

			function invokeFunction(func) {
				func();
			}

			invokeFunction(argumentFunction);


			// function as argument as return keyword

			function returnFunction() {
				let name = "Chris";
				return name;
			}

			function invokedFunction(func) {
				console.log(func());
			}

			invokedFunction(returnFunction);

		// Using multiple  parameters

			// multiple 'arguments' will correspond to the number of "parameters" declared in a function in succeeding order.

			function createFullName(firstName, middleName, lastName) {
				console.log(firstName + " " + middleName + " " + lastName);
			}

			createFullName("Juan", "Dela", "Cruz");

			// in JavaScript, providing more/less arguments than the expect will not return an error

			// providing less arguments than the expect parameters will automatically assign an undefined value to the parameter.

			createFullName("Gemar", null, "Cruz");

			createFullName('Lito', 'Masbate', "Galan", "jr.");

			//In other programming languages, this will return an error stating that "the number of arguments do not match the number of parameters".

			// Using variable as Multiple Arguments
			let firstName = "John";
			let middleName = "Doe";
			let lastName = "Smith";

			createFullName(firstName, middleName, lastName);

			// alert - notif
			// prompt - with user input

			// Using Alert()
			// alert () allows us to show a small window at the top of our browser page to show information to our users. as opposed to console.log which only shows the message on the console.

			// alert("Hello World!") //this will run immediately when the page loads
			//syntax: alert("messageInString");

			function showSampleAlert() {
				alert("hello, user!");
			}

			// showSampleAlert();

			console.log("I will only log in the console when the alert is dismissed.");

			//using prompt()
			// prompt() allows us to show a small window at the top of the browser to gather user input.
			//the value gathered from a prompt is returned as a string
			//syntax: prompt("dialogInString")
			// let samplePrompt = prompt("Enter your name.");
			// console.log("Hello, " + samplePrompt);
			// console.log(typeof samplePrompt);

			// let age = prompt("Enter your age:")
			// console.log(age);
			// console.log(typeof age);

			// returns an empty string when there is no input or null if the user cancels the prompt.
			// let sampleNullPromt = prompt('dont enter anything.');
			// console.log(sampleNullPromt);
			// console.log(typeof sampleNullPromt);

			function printWelcomeMessage() {
				let firstName = prompt("enter your first name.");
				let lastName = prompt("enter your last name.");

				console.log("Hello, " + firstName + " " + lastName + "!");
				console.log("Welcome to my page!");
			}

			printWelcomeMessage();





