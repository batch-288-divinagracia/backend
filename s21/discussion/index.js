// console.log("Good Morning Batch 288!")

// an array in programming is simply a list of data that share the same data type.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";


// NOW, WITH AN ARRAY, WE CAN SIMPLY WRITE THE CODE ABOVE LIKE THIS


let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; 

// [SECTION] ARRAYS
	// ARRAYS ARE USED TO STORE MULTIPLE RELATED VALUES IN A SINGLE VARIABLE.
	//  THEY ARE DECLARED USING THE SQUARE BRACKETS ([]) ALSO KNOW AS "ARRAY LITERALS"
	/*
		SYNTAX:
			let/const arrayName = [elemenA, elementB, elementC . . . .];

	*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// POSSIBLE USE OF AN ARRAY BUT IS NOT RECOMMENDED

let mixedArr = [12, 'Asus', null, undefined, {}]

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);


// ALTERNATIVE WAY TO WRITE ARRAYS:
let myTask = [
	'drink html',
	'eat javacript',
	'inhale css',
	'bake sass'];

console.log(myTask);

// CREATING AN ARRAY WITH VALUES FROM VARIABLE.
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";
city1 = "Osaka";
let cities = [city1, city2, city3];


console.log(cities);

// [SECTION] LENGTH PROPERTY
	// THE .length PROPERTY ALLOWS US TO GET AND SET THE TOTAL NUMBER OF ITEMS IN AN ARRAY

console.log(myTask.length);
console.log(cities.length);


let blankArr = [];
console.log(blankArr.length);

// .length PROPERTY CAN ALSO SET THE TOTAL NUMBER OF ELEMENTS IN AN ARRAY, MEANING WE CAN ACTUALLY DELETE THE LAST ITEM IN THE ARRAY OF SHORTEN THE ARRAY BY SIMPLE UPDATING THE LENGTH OF AN ARRAY

console.log(myTask);
// myTask.length = myTask.length -1;

myTask.length -= 2;
console.log(myTask);
console.log(myTask.length);

// TO DELETE A SPECIFIC ITEM IN AN ARRAY WE CAN EMPLOY ARRAY METHODS (WHICH WILL BE SHOWN/DISCUSSED IN THE NEXT SESSION) OR AN ALGORITHM.
console.log(cities);
cities.length--;
console.log(cities);


// WE CAN'T DO THE SAME ON STRINGS
let fullName = "Jamie Noble";
console.log(fullName);
fullName.length -= 1;
console.log(fullName);

	// IF YOU CAN SHORTEN THE ARRAY BY SETTING THE LENGTH PROPERTY, YOU CANNOT SHORTEN THE NUMBER OF CHARACTERS USING THE SAME PROPERTY.

	// SINCE YOU CAN SHORTEN THE ARRAY BY SETTING THE LENGTH PROPERTY, YOU CAN ALSO LENGTHEN IT BY ADDING A NUMBER INTO THE LENGTH PROPERTY. SINCE WE LENGTHEN THE ARRAY FORCIBLY, THERE WILL BE ANOTHER ITEM IN THE ARRAY HOWEVER IT WILL BE EMPTY OR UNDEFINED.

	let theBeatles = ["John", "Ringo", "Paul", " George"];
	console.log(theBeatles);

	theBeatles.length += 1;
	console.log(theBeatles);

// [SECTION] READING FROM ARRAYS
		/*
			-ACCESSING ARRAY ELEMENTS IS ONE OF THE MORE COMMON TASK TAHT WE CAN DO WITH AN ARRAY
			- WE CAN DO THIS  BY USING THE ARRAY INDEXES.
			- EACH ELEMENT IS AN ARRAY IS ASSOCIATED WITH IT'S OWN INDEX/NUMBER.	

			SYNTAX:
				arrayName[index]

		*/

console.log(grades[0])
	let lakerLengends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

	console.log(lakerLengends);
	// YOU CAN ACTUALLY SAVE/STORE ARRAY ITEMS IN ANOTHER VARIABLE

	let currentLakers = lakerLengends[2];
	console.log(currentLakers);

	// YOU CAN ALSO REASSIGN ARRAY VALUES USING ITEMS INDICES

	console.log("Array before reassignment:");
	console.log(lakerLengends);

	//REASSIGN THE VALUE OF SPECIFIC ELEMENT:
	lakerLengends[1] = "Pau Gasol";
	console.log("Array After reassignment:") 
	console.log(lakerLengends)

	// ACCESSING THE LAST ELEMENT OF AN ARRAY
	// SINCE THE FIRST ELEMENT OF AN ARRAY STARTS AT 0, SUBTRACTING 1 TO THE LENGTH OF AN ARRAY WILL OFFSET THE VALUE BY ONE ALLOWING US TO GET THE LAST ELEMENT.

	let bullsLegends = ["Jordan", "Pippen", "Rodman",
		"Rose", "Kukoc"];
	

	console.log(bullsLegends[bullsLegends.length-1]);

	//ADDING ELEMENTS INTO ARRAY

	let newArr = [];
	console.log(newArr)

	newArr[newArr.length] = "Cloud strife";
	console.log(newArr);

	newArr[newArr.length] = "tifa Lockhart";
	console.log(newArr);

	// [SECTION] LOOPING OVER AN ARRAY
		//  YOU CAN USE A FOR LOOP TO ITERATE OVER ALL ITEMS IN AN ARRAY

console.log("---------------------------------");
		// FOR LOOP
		for(let index = 0; index < bullsLegends.length; index++){
			console.log(bullsLegends[index]);
		}

		// Example:
			// WE ARE GOING TO CREATE A LOOP THAT WILL CHECK WHETHER THE ELEMENT INSIDE OUR ARRAY IS DIVISIBLE BY 5 OR NOT.
		// IF DIVISIBLE BY 5 CONSOLE NUMBER + "IS DIVISIBLE BY 5"
		// IF NOT CONSOLE THE NUMBER + "IS NOT DIVISIBLE BY 5"
		let numArr = [5,12,30, 46, 40];

		let numDivBy5 = [];
		let numNotDivBy5 = [];

		for (let i = 0; i < numArr.length; i++) {
			// IF STATEMENT TO FILTER WHETHER THE NUMBER IS DIVISIBLE BY 5 OR NOT.
			if(numArr[i] % 5 === 0){
				console.log(numArr[i] + " is divisible by 5");
				numDivBy5[numDivBy5.length] = numArr[i];
			} else{
				console.log(numArr[i] + " is not divisible by 5");
				numNotDivBy5[numNotDivBy5.length] = numArr[i];
			}
		}

		console.log(numDivBy5);
		console.log(numNotDivBy5);

		// [SECTION] MULTIDIMENSIONAL ARRAY
		// MULTIDIMENSIONAL ARRAYS ARE USEFUL FOR STORING COMPLEX DATA STRUCTURE
		//  THOUGH USEFUL IN NUMBER OF CASES, CREATING COMPLEX ARRAY STRUCTURE IS NOT ALWAYS RECOMMENDED.

		let chessboard = [['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
			['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
			['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
			['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
			['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
			['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
			['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
			['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
			]

console.log(chessboard);

console.log(chessboard[0][2]);
console.log(chessboard[3][4]);
console.log(chessboard[7][3]);
console.table(chessboard);
