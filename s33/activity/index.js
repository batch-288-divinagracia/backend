//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}



// Getting all to do list item
async function getAllToDo(){

   return await (

      //add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos', {
         method: "GET",
         headers: {
            'content-type': 'application/json'
         }
      })
      .then((response) => response.json())
      .then((json) => {
         return json.map(item => item.title)
      })


  );

}

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then((response) => response.json())
      .then((json) => json)


   );

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       //Add fetch here.
       fetch('https://jsonplaceholder.typicode.com/todos', {
         method: "POST",
         headers: {
            'content-type': 'application/json'
         },
         body: JSON.stringify({
            userId: 1,
            title: 'Create To Do List Item',
            id: 1,
            completed: false 
         })
       })
      .then((response) => response.json())
      .then((json) => json)


   );

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/2', {
         method: "PUT",
         headers:{
            'content-type': 'application/json'
         },
         body: JSON.stringify({
            Title: "Update To do List Item",
            Description: "To update the my to do list with different data structure",
            Status: "Pending",
            dateCompleted: "Pending",
            userId: 1
         })
      })
      .then((response) => response.json())
      .then((json) => json)


   );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/3', {
         method: "DELETE"
      })
      .then((response) => response.json())
      .then((json) => json)

   );

}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}

