// console.log(`Hello Batch 288`);

// [Section] JavaScript Synchronous vs Asynchronous
	// Javascript by default is synchronous meaning that only one statement is executed a time

	// this can be proven when a statement has an error, javascript will not proceed with the next statement
// console.log(`hello world`);

// conole.log(`hello after the world`);

// console.log(`Hello`);

	// when certain statements take a lot of time to process, this slows down out code.

	// for(let index = 0; index <= 1000000; index++){
	// 	console.log(index);
	// }

	// console.log("hello Again!");

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// [Section] Getting all post
	// the Fetch API allows you to asynchronously request for a resource data.
	// so it means the fetch method that we are going to use here will run asynchronously. 
	// Syntax:
		// fetch('URL');


	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		/*
			fetch('URL')
			.then((response => response));
		*/
	// Retrive all posts follow the REST API

	fetch('https://jsonplaceholder.typicode.com/posts')
	// the fetch method will return a promise that resolves the response object.

	// the "then" method captures the response object
	// Use the "Json" method from the response object to convert the data retrived into JSON format to be used in our application
	.then(response => response.json())
	.then(json => console.log(json))

	// the "async" and "await" keyword, it is another approach that can be used to achieve asynchronous code

	// Creates an asynchronous function

	async function fetchData(){

		let result = await fetch('https://jsonplaceholder.typicode.com/posts');

		console.log(result);

		let json = await result.json();

		console.log(json);
	}

	// fetchData();

	// [Section] Creating Getting Specific Post

	// retrieves specific post following the rest API (/post/:id)

	fetch('https://jsonplaceholder.typicode.com/posts/5')
	.then(response => response.json())
	.then(json => console.log(json));

	// [Section] Creating Post
	// Syntax:
		/*
			options is an object that contains the method, the header and the body of  the request

			by default if you don't add the method in the fetch request, it will be a GET method.

			fetch('URL', options)
			.then(response =. {})
			.then(response =. {})

		*/

		fetch('https://jsonplaceholder.typicode.com/posts', {
			// sets the method of the request object to post following the rest API
			method: 'POST',
			// sets the header data of the request object to be sent to the backend
			// specified that the content will be in JSON structure
			headers: {
				'content-type': 'application/json'
			},
			// sets the content/body data of the request object to be sent backend
			body: JSON.stringify({
							title: 'New post',
							body: 'Hello World',
							userId: 1
						})
		})

		// .then(response => response.json())
		// .then(json => console.log(json));


		// [Section] Update a specific Post

		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: "PUT",
			headers:{
				'content-type': 'application/json'
			},
			body: JSON.stringify({
							title: 'Update Post'
						})
		})

		.then(response => response.json())
		.then(json => console.log(json));


		// PATCH 
		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: "PATCH",
			headers:{
				'content-type': 'application/json'
			},
			body: JSON.stringify({
							title: 'Update Post'
						})
		})

		.then(response => response.json())
		.then(json => console.log(json));

		// The PUT method is a method of modifying resource where the client sends data that updates the entire objec/documents

		// Patch method applies a partial update to the object or document

		// [Section] Deleting a Post

		// deleting specific post following the REST API
		fetch('https://jsonplaceholder.typicode.com/posts/1', {method: "DELETE"})
		.then(response => response.json())
		.then(json => console.log(json));

		 fetch('https://jsonplaceholder.typicode.com/todos/', {
         method: "POST",
         headers: {
            'content-type': 'application/json'
         },
         body: JSON.stringify({
            userId: 1,
            title: 'Hello',
            id: 1,
            completed: true 
         })
       })
      .then(response => response.json())
      .then(json => json)



async function createToDo(){
   
   return await (

       //Add fetch here.
       fetch('https://jsonplaceholder.typicode.com/todos/', {
         method: "POST",
         headers: {
            'content-type': 'application/json'
         },
         body: JSON.stringify({
            userId: 1,
            title: 'Hello',
            id: 1,
            completed: true 
         })
       })
      .then(response => response.json())
      .then(json => json)


   );

}