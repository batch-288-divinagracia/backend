// show databases - list of the db inside our cluster
// use "dbName" to use a specific database
// show collections - to see the list of collections inside the db

// CRUD operation
/*
	- CRUD operation is the heart if any backend application
	- mastering the CRUD operation is essential for any developer expecoally to those who want to become backend developer
*/

// [Section] Inserting Document (Create)
	//  Insert one document
		/*
			Since mongoDB deals with object as it's structure for any documents we can easily create them by providing objects in our method/operation.

			Syntax:
				db.collectionName.InsertOne({
					object
				})
		*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	Contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "python"],
	department: "none"
});


// Insert Many

/*
	Syntax:
		db.collectionName.insertMany([{objectA}, {objectB}]);
*/

db.users.insertMany([
	{
		"firstName" : "Stephen",
		"lastName" : "Hawking",
		"age": 76,
		"Contact" : {
			"phone" : "87654321",
			"email" : "stephenhawking@gmail.com"
		},
		"courses" : ["pyhton", "React", "PHP"],
		"department" : "none"
	},

	{
		"firstName" : "Neil",
		"lastName" : "Armstrong",
		"age": 82,
		"Contact" : {
			"phone" : "87654321",
			"email" : "neilarmstrong@gmail.com"
		},
		"courses" : ["React", "Laravel", "Sass"],
		"department": "none"
	}
]);

// [Section] Finding documents (Read operation)
	// db.collectionName.find();
	//  db.collectionName.find({field:value});
// using the find() method, it will show you the list of all the documents inside our collection

// the "pretty" method wallows us to bew alble to view the documents returned by or terminals t obe in a better format
db.users.find();


// it will return the documents that will pass the criteria given in the method. 
db.users.find({firstName: "Stephen"});


db.users.find({_id : ObjectId("646c5b4ff6f31684c89959a5")});

// multiple criteria
db.users.find({lastName: "Armstrong", age:82});

db.users.find({firstName : "Jane", age : 22});

db.users.find({Contact: {
	phone: "123456789",
	email: "janedoe@gmail.com"
}});

db.users.find("Contact.phone" : "123456789");

// [Section] updating documents (Update)
db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// updateOne Method

/*
		Syntax:
		db.collectionName.updageOne({criteria, {$set: {field:value}}});
*/

db.users.updateOne(
	{ firstName: 'Bill'},
	{
	$set: {
		firstName: "Chirs"
		}
	}
);

db.users.updateOne(
	{ firstName: "Jane"},
	{
		$set: {
			lastName: "Edited"
		}
	}
);

// Updating multiple documents
/*
		Syntax:
			db.collectionName.updateMany(
			{Criteria},
			{
				$set; {
					{field: value}
				}
			}
		)
*/

 db.users.updateMany(
 	{ department: "none"},
 	{
 		$set : {
 			department: "HR"
 		}
 	}
 );

 // Replace One
 	/*
		Syntax: db.collectionName.replaceOne(
		{Criteria}, 
		{
			$set : {
				object
				}
			}
		)
 	*/

 db.users.insertOne({firstName: "test"});

 db.users.replaceOne(
 	{firstName: "test"},
 	{
 		firstName: "Bill",
 		lastName: "Gates",
 		age: 65,
 		contact: {},
 		courses: [],
 		department: "Operations"
 	}
 )

 // [Section] Deleting Documents

 	/*
		db.collectionName.deleteOne({criteria})
 	*/

db.users.deleteOne({firstName: "Bill"});


 // Deleting multiple Document
 	/*
		db.collectionName.deleteMany({criteria})
 	*/

db.users.deleteMany({firstName: "Jane"});

// All the documents in our collection will be deleted
db.users.deleteMany({});


